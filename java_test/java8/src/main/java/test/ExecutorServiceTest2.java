package test;

import java.util.List;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Future;

public class ExecutorServiceTest2 {
    public static void main(String args[]) {
        final int maxCore = Runtime.getRuntime().availableProcessors();
        final ExecutorService executor = Executors.newFixedThreadPool(maxCore);
        final List<Future<String>> futures = new ArrayList<>();

        for (int i = 1; i < 5; i++) {
            futures.add(executor.submit(() -> {
                final int index = i;
                return "job" + index + " " + Thread.currentThread().getName();
            }));
        }

        for (Future<String> future: futures) {
            String result = future.get();
            System.out.println(result);
        }

        executor.shutdownNow();
        System.out.println("end");
    }
}

