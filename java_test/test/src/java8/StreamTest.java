package java8;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

public class StreamTest {

    public static void main(String args[]) {
//        Stream<String> stream1 = Stream.of("code", "chacha", "blog", "example");
//        Stream<String> stream2 = Stream.empty();
//        Stream<String> stream3 = Stream.generate(() -> "Echo").limit(5);
//        Stream<Double> stream4 = Stream.generate(Math::random).limit(5);
//        Stream<Integer> stream5 = Stream.iterate(0, n -> n + 2).limit(5);
//
//        System.out.println("stream1");
//        stream1.forEach(s -> System.out.print(s + " "));
//
//        System.out.println("");
//        System.out.println("stream2");
//        stream2.forEach(s -> System.out.print(s + " "));
//
//        System.out.println("");
//        System.out.println("stream3");
//        stream3.forEach(System.out::print);
//
//        System.out.println("");
//        System.out.println("stream4");
//        stream4.forEach(s -> System.out.print(s + " "));
//
//        System.out.println("");
//        System.out.println("stream5");
//        stream5.forEach(s -> System.out.print(s + " "));

        System.out.println("# filter");
        List<String> list =
                Arrays.asList("a1", "a2", "b1", "b2", "c2", "c1", "c3");
        Stream<String> stream1 = list.stream();
        stream1.filter(s -> s.startsWith("c")).forEach(System.out::println);

        System.out.println("# filter + map");
        Stream<String> stream2 = list.stream();
        stream2.filter(s -> s.startsWith("c"))
                .map(String::toUpperCase).forEach(System.out::println);

        System.out.println("# flatMap");
        String[][] list2 = new String[][]{{"a1", "a2"}, {"b1", "b2"}, {"c1", "c2", "c3"}};
        Stream<String[]> stream3 = Arrays.stream(list2);
        Stream<String> stream4 = stream3.flatMap(s -> Arrays.stream(s));
        stream4.forEach(System.out::println);


        System.out.println("# flatMap + filter");
        Stream<String[]> stream5 = Arrays.stream(list2);
        Stream<String> stream6 = stream5.flatMap(s -> Arrays.stream(s));
        stream6.filter(s-> s.startsWith("a")).forEach(System.out::println);






    }
}
