package stream;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

public class StreamExample2 {
    public static void main(String args[]) {

        System.out.println("# filter");
        List<String> list =
                Arrays.asList("a1", "a2", "b1", "b2", "c2", "c1", "c3");
        Stream<String> stream1 = list.stream();
        Stream<String> filtered = stream1.filter(s -> s.startsWith("c"));
        filtered.forEach(System.out::println);

        System.out.println("# map");
        Stream<String> stream2 = list.stream();
        stream2.map(s -> s.toUpperCase()).forEach(System.out::println);

        System.out.println("# map");
        Stream<String> stream3 = list.stream();
        stream3.map(String::toUpperCase).forEach(System.out::println);

        System.out.println("# flatMap");
        String[][] arrays = new String[][]{{"a1", "a2"}, {"b1", "b2"}, {"c1", "c2", "c3"}};
        Stream<String[]> stream4 = Arrays.stream(arrays);
        Stream<String> stream5 = stream4.flatMap(s -> Arrays.stream(s));
        stream5.forEach(System.out::println);

        System.out.println("# flatMap + filter + map");
        Stream<String[]> stream6 = Arrays.stream(arrays);
        Stream<String> stream7 = stream6.flatMap(s -> Arrays.stream(s));
        stream7.filter(s-> s.startsWith("a"))
                .map(String::toUpperCase).forEach(System.out::println);

    }
}
