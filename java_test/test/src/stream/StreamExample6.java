package stream;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Stream;

public class StreamExample6 {
    public static void main(String args[]) {
        // sort
        List<String> langs =
                Arrays.asList("java", "kotlin", "haskell", "ruby", "smalltalk");
        System.out.println("sorted:");
        langs.stream().sorted()
                .forEach(System.out::println);

        System.out.println("reversed:");
        langs.stream().sorted(Comparator.reverseOrder())
                .forEach(System.out::println);

        System.out.println("sorted:");
        langs.stream().sorted(Comparator.comparing(String::length))
                .forEach(System.out::println);
        System.out.println("reversed:");
        langs.stream().sorted(Comparator.comparing(String::length).reversed())
                .forEach(System.out::println);

        System.out.println("sorted:");
        final Comparator<String> comp = (p1, p2) -> Integer.compare( p1.length(), p2.length());
        langs.stream().sorted(comp)
                .forEach(System.out::println);

        System.out.println("reversed:");
        langs.stream().sorted(comp.reversed())
                .forEach(System.out::println);


        List<MyString> langs2 =
                Arrays.asList(new MyString("java"), new MyString("kotlin"),
                        new MyString("haskell"), new MyString("ruby"),
                        new MyString("smalltalk"));

        langs2.stream().sorted()
                .forEach(System.out::println);

    }

    public static class MyString implements Comparable<MyString> {
        public String str;

        public MyString(String str) {
            this.str = str;
        }

        @Override
        public int compareTo(MyString o) {
            return Integer.compare(this.str.length(), o.str.length());
        }

        @Override
        public int hashCode() {
            return str.hashCode();
        }

        @Override
        public String toString() {
            return str;
        }

    }

}
