package stream;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class StreamExample8 {
    public static void main(String args[]) {

        List<String> elements =
                Arrays.asList("a", "a1", "b", "b1", "c", "c1");
        Optional<String> firstElement = elements.stream()
                .filter(s -> s.startsWith("b")).findFirst();
        Optional<String> anyElement = elements.stream()
                .filter(s -> s.startsWith("b")).findAny();
        firstElement.ifPresent(System.out::println);
        anyElement.ifPresent(System.out::println);


        firstElement = elements.stream().parallel()
                .filter(s -> s.startsWith("b")).findFirst();
        anyElement = elements.stream().parallel()
                .filter(s -> s.startsWith("b")).findAny();

        firstElement.ifPresent(System.out::println);
        anyElement.ifPresent(System.out::println);


        boolean anyMatch
                = elements.stream().anyMatch(s -> s.startsWith("b"));
        System.out.println("anyMatch: " + (anyMatch ? "true" : "false"));

        boolean allMatch
                = elements.stream().allMatch(s -> s.startsWith("b"));
        System.out.println("allMatch: " + (allMatch ? "true" : "false"));

        boolean noneMatch
                = elements.stream().noneMatch(s -> s.startsWith("b"));
        System.out.println("noneMatch: " + (noneMatch ? "true" : "false"));

    }
}
