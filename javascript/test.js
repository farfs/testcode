// for (var i = 0; i < 10; i++) {
//     console.log("Hello, World!" + i);
// }

function print(msg) {
    console.log(msg)
}

var color ="blue";
function changeColor() {
    if (color === "blue") {
        color = "red";
    } else {
        color = "blue";
    }
}

//changeColor()
//print(color)


function changeColor2() {
    var anotherColor = "red";
    function swapColor() {
        var tempColor = anotherColor;
        anotherColor = color;
        color = tempColor;
    }
    swapColor();
}

// changeColor2();

function compare(value1, value2) {
    if (value1 < value2) {
        return -1;
    } else if (value1 > value2) {
        return 1;
    } else {
        return 0;
    }
}
var values = [0,15,5,10,1]
// print(values.sort(compare));


var numbers = [1,2,3,4,5,6,7,8,9];

var filterResult = numbers.filter(function(item, index, array) {
    return (item > 2);
});

// print(filterResult);

var someData = new Date(Date.UTC(2000,0))





// var Dictionary = require("oxford-dictionary-api");
// var app_id = "e1fbc006";
// var app_key = "e9858b7540131ca9270dbf6206e8b06c";
// var dict = new Dictionary(app_id, app_key);
// dict.find("ace",function(error,data){if(error) return console.log(error); console.log(data); });

// var Dictionary = require("oxford-dictionary");

// var config = {
//     app_id : "e1fbc006",
//     app_key : "e9858b7540131ca9270dbf6206e8b06c",
//     source_lang : "en"
// };

// var dict = new Dictionary(config);
// var lookup = dict.find("ace");

// lookup.then(function(res) {
//   console.log(res);
// });

var https = require('https');

var findword = function(language, word_id, app_id, app_key) {
    var path = 'https://od-api.oxforddictionaries.com:443/api/v1/entries/'  + language + '/'  + word_id;
    var options = new OptionObj(path, app_id, app_key);
    return buildRequest(options);
} // .find

// Constructor Function for Option Objects
function OptionObj(path, app_id, app_key) {
    var options = {
        host :  'od-api.oxforddictionaries.com',
        port : 443,
        path : path,
        method : 'GET',
        headers : {
            "Accept": "application/json",
            "app_id": app_id,
            "app_key": app_key
            }
        };
    return options;
} // end OptionObj


var buildRequest = function(options) {
    return new Promise(function(resolve, reject) {
        https.get(options, function(res) {
            if (res.statusCode == 404) {
                return reject("No such entry found."); 
            }
            var data = "";
            
            res.on('data', function (chunk) {
                data += chunk;
            });

            res.on('end', function() {
                var result;
                try {
                    result = JSON.parse(data);
                } catch (exp) {
                    result = {
                        'status_code': 500,
                        'status_text': 'JSON Parse Failed'
                    };
                    reject(result);
                }
                resolve(result);                    
            });

            res.on('error', function (err) {
                reject(err);
            });                
        }); // end https.get
    }); // end promise
}; // end buildRequest


// var lookup = findword("en", "ace", "e1fbc006", "e9858b7540131ca9270dbf6206e8b06c");
// lookup.then(function(res) {
//   console.log(res.results.values());
// },
// function(err) {
//   console.log(err);
// });


// const countdown = (value, fn, delay=1000) => {
//     fn(value)
//     return (value > 0) ?
//         setTimeout(() => countdown(value-1, fn), delay) :
//         value
// }


// const log = value => console.log(value)
// countdown(10, log);
// print("finished")


var dan = {
    type: "person",
    data: {
        gender: "male",
        info: {
            id: 22,
            fullname: {
                first: "Dan",
                last: "Deacon"
            }
        }
    }
}


const deepPick = (fields, object={}) => {
    const [first, ...remaining] = fields.split(".")
    return (remaining.length) ? 
        deepPick(remaining.join("."), object[first]) :
        object[first]
}

print(deepPick("type", dan));
print(deepPick("data.info.fullname.first", dan));
print(deepPick("data.info.fullname.last", dan));
