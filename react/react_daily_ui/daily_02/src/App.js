// https://www.fullstackreact.com/react-daily-ui/002-checkout/
import React, { Component } from 'react';
import logo from './logo.svg';
import './App.scss';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';


class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      mounted: false,
      people: 1,
      price: 320.00,
      tax: 20,
      duration: 5,
      discount: 5,
    }

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);

  }

  componentDidMount() {
    this.setState({mounted: true});
  }

  handleSubmit(e) {
    e.preventDefault();
  }

  handleChange(e) {
    console.log(e.target.value);
    this.setState({duration: e.target.value});
  }

  render() {
    var overlay, container;
    if (this.state.mounted) {
      overlay = (
        <Overlay image="https://s3-us-west-2.amazonaws.com/s.cdpn.io/557257/jj-2.jpg"/>
      );
      container = (
        <Container>
          <ImagePreview price={this.state.price} duration={this.state.duration}
            people={this.state.people} image="https://s3-us-west-2.amazonaws.com/s.cdpn.io/557257/jj-2.jpg" />
          <Checkout discount={this.state.discount}
            tax={this.state.tax} price={this.state.price}
            duration={this.state.duration} onSubmit={this.handleSubmit} />
        </Container>
      );
    }

    return (
      <div className="App">
        <ReactCSSTransitionGroup 
          transitionName="overlay"
          transitionEnterTimeout={500}
          transitionLeaveTimeout={300}>
          {overlay}
        </ReactCSSTransitionGroup>
        <ReactCSSTransitionGroup 
          transitionName="container"
          transitionEnterTimeout={500}
          transitionLeaveTimeout={300}>
          {container}
        </ReactCSSTransitionGroup>
        <Header onChange={this.handleChange} />
      </div>
    );
  }
}

class Header extends Component {
  render() {
    return (
      <header>
        <input onChange={this.props.onChange}
            type="range"
            max="100"
            min="1"
            step="1" />
      </header>
    )
  }
}

class Overlay extends Component {
  render() {
    return (
      <div 
        className="Overlay" 
        style={{'backgroundImage':'url(' + this.props.image + ')'}}>
        Something
      </div>
    );
  }
}

class Container extends Component {
  render() {
    return (
      <div className="Container">
        {this.props.children}
      </div>
    );
  }
}

class ImagePreview extends Component {
  render() {
    return (
      <div className="ImagePreview" 
          style={{'backgroundImage':'url(' + this.props.image + ')'}}>
        <div className="WorkspaceOverview">
          <WorkspaceInformation 
            name = "Coworking Space, South Korea" 
            price={this.props.price}
            duration={this.props.duration} />
        </div>
      </div>
    );
  }
}

class WorkspaceInformation extends Component {
  render() {
    if (this.props.duration > 1) {
      var duration = this.props.duration + ' days';
    } else {
      var duration = this.props.duration + ' day';
    }

    return (
      <div className="WorkspaceInformation">
        <div className="WorkspaceName">{this.props.name}</div>
        <div className="WorkspacePrice">
          <div className="Price">{this.props.price} GBP </div>
          <div className="Duration">/ {duration}</div>
        </div>
      </div>
    );
  }
}

class Checkout extends Component {
  render() {
    return (
      <div className="Checkout">
        <OrderSummary 
          discount={this.props.discount}
          tax={this.props.tax}
          price={this.props.price}
          duration={this.props.duration} />
        <PaymentForm onSubmit={this.props.onSubmit} />
      </div> 
    )
  }
}

class OrderSummary extends Component {
  render() {
    if (this.props.duration > 1) {
      var duration = this.props.duration + ' days';
    } else {
      var duration = this.props.duration + ' day';
    }

    var initialTotal = this.props.duration * this.props.price;
    var discount = Math.floor((initialTotal / 100) * this.props.discount);
    var subTotal = initialTotal - discount;
    var tax = Math.floor((subTotal / 100) * this.props.tax);
    var total = subTotal + tax;

    return (
      <div className="OrderSummary">
        <div className="Title">Order Summary</div>
        <table>
          <tr>
            <td>{this.props.price} x {duration}</td>
            <td>{initialTotal} GBP</td>
          </tr>
          <tr>
            <td>Discount</td>
            <td>{discount} GBP</td>
          </tr>
          <tr>
            <td>Subtotal</td>
            <td>{subTotal} GBP</td>
          </tr>
          <tr>
            <td>Tax</td>
            <td>{tax} GBP</td>
          </tr>
        </table>
        <div className="Total">
          <div className="TotalLabel">Total</div>
          <div className="Amount">
            {total} <small>GBP</small>
          </div>
        </div>
      </div>
    )
  }
}

class PaymentForm extends Component {
  render() {
    return(
      <div className="PaymentForm">
        <form onSubmit={this.props.onSubmit}>
          <div className="Title">Payment information</div>
          <BasicInput 
            name="name"
            label="Name on credit card"
            type="text"
            placeholder="John Smith" />
          <BasicInput 
            name="card"
            label="Credit card number"
            type="number"
            placeholder="0000 0000 0000 0000" />
          <ExpiryDate />
          <CheckoutButton />
        </form>
      </div>
    );
  }
}

class CheckoutButton extends Component {
  render() {
    return (
      <div className="CheckoutButton">
        <button>Book securely</button>
        <span><i className="fa fa-fw fa-lock"></i> Your credit card information is encrypted</span>
      </div>        
    );
  }
}

class ExpiryDate extends Component {
  render() {
    return (
      <div className="ExpiryDate">
        <div>
          <label>Expires on</label>
          <div className="Expiry">
            <select>
              <option value="">January</option>
              <option value="">February</option>
              <option value="">March</option>
              <option value="">April</option>
              <option value="">May</option>
              <option value="">June</option>
              <option value="">July</option>
              <option value="">August</option>
              <option value="">September</option>
              <option value="">October</option>
              <option value="">November</option>
              <option value="">December</option>
            </select>
            <select name="" id="">
              <option value="">2016</option>
              <option value="">2017</option>
              <option value="">2018</option>
              <option value="">2019</option>
              <option value="">2020</option>
              <option value="">2021</option>
            </select>
          </div>
        </div>
        <div className="CVCField">
          <label>CVC</label>
          <input placeholder="000" type="number" />
        </div>
      </div>
    );
  }
}

class BasicInput extends Component {
  render() {
    return (
      <div className="BasicInput">
        <label for={this.props.name}>{this.props.label}</label>
        <input id={this.props.name}
            type={this.props.type}
            placeholder={this.props.placeholder} />
      </div>
    )
  }
}

export default App;
