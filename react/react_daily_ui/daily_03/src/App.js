import React, { Component } from 'react';
import logo from './logo.svg';
import './App.scss';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';


class App extends Component {
  getInitialState() {
    return { mounted: false };
  }

  constructor(props) {
    super(props);

    this.state = {
      mounted: false,
    }
  }

  componentDidMount() {
    this.setState({mounted: true});
  }

  handleSubmit(e) {
    this.setState({mounted: false});
    e.preventDefault();
  }

  render() {
    var child;

    if (this.state.mounted) {
      child = (<Modal onSubmit={this.handleSubmit} />);
    }

    return (
      <div className="App">
        <ReactCSSTransitionGroup
          transitionName="example"
          transitionEnterTimeout={500}
          transitionLeaveTimeout={300}>
            {child}
        </ReactCSSTransitionGroup>
      </div>
    );
  }
}

class Modal extends Component {
  render() {
    return (
      <div className="Modal">
        <form 
          onSubmit={this.props.onSubmit}
          className="ModalForm"
        >
          <Input 
            id="name"
            type="text"
            placeholder="Jack-Edward Oliver"
          />
          <Input 
            id="username"
            type="email"
            placeholder="farfs@gmail.com"
          />
          <Input 
            id="password"
            type="password"
            placeholder="password"
          />
          <button>Log in <i className="fa fa-fw fa-chevron-right"></i>
          </button>
        </form>
      </div>
    );
  }
}

class Input extends Component {
  render() {
    return (
      <div className="Input">
        <input 
          id={this.props.name} 
          autocomplete="false" 
          required type={this.props.type}
          placeholder={this.props.placeholder} 
        />
        <label for={this.props.name}></label>
      </div>
    );
  }
}

export default App;
