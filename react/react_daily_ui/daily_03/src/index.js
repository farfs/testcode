//https://www.fullstackreact.com/react-daily-ui/001-sign-up-form/
// scss : https://medium.com/@Connorelsea/using-sass-with-create-react-app-7125d6913760
//          https://junhobaik.github.io/js-react-apply-sass/
import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(<App />, document.getElementById('root'));
registerServiceWorker();
